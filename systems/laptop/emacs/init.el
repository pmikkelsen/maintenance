;;; init.el --- My personal emacs configuration

;;; Commentary:
;; This file should be put in the ~/.emacs.d/ folder, but a symlink is OK.
;; To make a symlink, do the following:
;;     ln -sv /path/to/this/file ~/.emacs.d/init.el

;;; Code:

;; Enable use-package so I can configure all other packages with that.
(require 'use-package)

;; Disable the splash screen
(setq inhibit-splash-screen t)

;; Set the editor theme and font
(load-theme 'atom-one-dark t)
(set-frame-font "Inconsolata-14" t t)

;; Configure the scratch buffer
(setq initial-major-mode      'org-mode)
(setq initial-scratch-message "Done loading hehe..")

;; Put autosaves in ~/.emacs.d/autosaves
(setq backup-directory-alist `(("." . "~/.emacs.d/autosaves")))

;; Disable tabs
(setq-default indent-tabs-mode nil)

;; Set some basic info about me
(setq user-mail-address "peter@alaok.org"
      user-full-name    "Peter Mikkelsen")

;; Display battery in the mode line
(use-package battery
  :custom
  (battery-mode-line-format "%p%% ")
  (display-battery-mode t))

;; Display time in the mode line
(use-package time
  :custom
  (display-time-default-load-average nil)
  (display-time-mode t))

;; Turn of the different bars
(use-package menu-bar
  :config
  (menu-bar-mode -1))

(use-package scroll-bar
  :config
  (scroll-bar-mode -1))

(use-package tool-bar
  :config
  (tool-bar-mode -1))

;; Linum mode
(use-package linum
  :config
  (line-number-mode -1)
  :hook
  ((prog-mode . linum-mode)
   (text-mode . linum-mode)))

;; Ido
(use-package ido
  :config
  (ido-mode t))

;; Org-mode
(use-package org
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)
     (emacs-lisp . t)
     (scheme . t)))
  :custom
  (org-src-fontify-natively   t)
  (org-src-tab-acts-natively  t)
  (org-confirm-babel-evaluate nil))

;; Geiser mode
(use-package geiser
  :custom
  (geiser-default-implementation 'guile)
  (geiser-active-implementations '(guile))
  (geiser-mode-start-repl-p      t))

;; CC-mode
(use-package cc-mode
  :custom
  (c-default-style "gnu")
  (basic-offset    2))

;; Paredit
(use-package paredit
  :hook ((emacs-lisp-mode . enable-paredit-mode)
         (lisp-mode       . enable-paredit-mode)
         (scheme-mode     . enable-paredit-mode)))

;; PDF-tools
(use-package pdf-tools
  :config
  (pdf-tools-install))

;; Mu4e and mail settings
(use-package mu4e
  :bind ("s-m" . mu4e)
  :custom
  (mail-user-agent       'mu4e-user-agent)
  (mu4e-maildir          "~/Maildir")

  (message-send-mail-function   'smtpmail-send-it)
  (smtpmail-stream-type         'starttls)
  (smtpmail-default-smtp-server "mail.alaok.org")
  (smtpmail-smtp-server         "mail.alaok.org")
  (smtpmail-smtp-service        587)

  (message-kill-buffer-on-exit t)

  (display-time-use-mail-icon t)
  (display-time-mail-directory "~/Maildir/INBOX/new"))

;; Epa
(use-package epa
  :config
  (epa-file-enable)
  :custom
  (epa-pinentry-mode 'loopback))

;; Magit
(use-package magit
  :bind (("C-x g"   . magit-status)
         ("C-x M-g" . magit-dispatch-popup)))

;; exwm
(use-package exwm
  :init
  (shell-command "setxkbmap dk")
  :custom
  (exwm-workspace-number 4)
  :config
  (exwm-input-set-key (kbd "s-r") #'exwm-reset)
  (exwm-input-set-key (kbd "s-w") #'exwm-workspace-switch)
  (exwm-input-set-key (kbd "s-&")
		      (lambda (command)
		        (interactive (list (read-shell-command "$ ")))
		        (start-process-shell-command command nil command)))
  (exwm-enable))

(use-package exwm-config
  :config
  (exwm-config-ido))

;; YASnippet
(use-package yasnippet
  :custom
  (yas-snippet-dirs '("~/src/guix/etc/snippets"))
  :config
  (yas-global-mode 1))

;; Flycheck
(use-package flycheck
  :config
  (global-flycheck-mode)

  :custom
  (flycheck-emacs-lisp-load-path 'inherit))

;; Company-mode
(use-package company
  :config
  (global-company-mode))

(provide 'init)
;;; init.el ends here

