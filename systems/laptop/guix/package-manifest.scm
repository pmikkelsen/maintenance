(define-module (systems laptop guix package-manifest)
  #:use-module (gnu packages))

(define development-tools
  '("git"
    "make"))

(define emacs
  '("emacs"
    "emacs-atom-one-dark-theme"
    "emacs-company"
    "emacs-flycheck"
    "emacs-guix"
    "emacs-org"
    "emacs-org-contrib"
    "emacs-paredit"
    "emacs-pdf-tools"
    "emacs-use-package"
    "emacs-yasnippet"
    "magit"
    "pinentry-emacs"))

(define email
  '("mu"
    "offlineimap"))

(define fonts
  '("font-inconsolata"))

(define games
  '("minetest"))

(define misc
  '("gnupg"
    "htop"
    "icecat"
    "openssh"
    "pavucontrol"
    "rsync"
    "setxkbmap"
    "strace"
    "unzip"
    "wget"))

(define programming
  '("gcc-toolchain"
    "guile"))

(specifications->manifest
 (append development-tools
         emacs
         email
         fonts
         games
         misc
         programming))
