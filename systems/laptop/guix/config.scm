(use-modules (gnu)
	     (gnu system nss)
	     (srfi srfi-1))

(use-service-modules desktop base ssh networking mcron)
(use-package-modules certs emacs openbox mail)

(define %authorized-guix-keys
  '(%alaok-guix-signing-key
    %hydra.gnu.org-guix-signing-key
    %berlin.gnu.org-guix-signing-key))

(define %substitute-urls
  (list "https://berlin.guixsd.org"
	"https://mirror.hydra.gnu.org"))

(define offlineimap-job
  #~(job '(next-minute)
         (string-append #$offlineimap "/bin/offlineimap")
         #:user "peter"))

(define %services
  (cons*
   (service connman-service-type)
   (service openssh-service-type
	    (openssh-configuration
	     (permit-root-login 'without-password)))
   (extra-special-file "/usr/bin/env" (file-append coreutils "/bin/env"))
   (mcron-service (list offlineimap-job))
   (remove (lambda (service)
	     (eq? (service-kind service) network-manager-service-type))
	   (modify-services
	    %desktop-services
	    (guix-service-type config =>
			       (guix-configuration
				(inherit config)
				(authorized-keys
				 %authorized-guix-keys)
				(substitute-urls
				 %substitute-urls)))))))

(operating-system
  (host-name "guixsd")
  (timezone "Europe/Copenhagen")
  (locale "en_US.UTF-8")
  (bootloader (bootloader-configuration
	       (bootloader grub-bootloader)
	       (target "/dev/sda")))
  (file-systems (cons* (file-system
			(device (file-system-label "guixsd-root"))
			(mount-point "/")
			(type "ext4"))
		       (file-system
			(device (file-system-label "guixsd-home"))
			(mount-point "/home")
			(type "ext4"))
		       %base-file-systems))
  (users (cons* (user-account
		 (name "peter")
		 (comment "Peter Mikkelsen")
		 (group "users")
		 (supplementary-groups '("wheel" "netdev"
					 "video" "cdrom"))
		 (home-directory "/home/peter"))
		%base-user-accounts))
  (packages (cons* nss-certs
		   emacs-exwm
		   openbox
		   %base-packages))
  (services %services)

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
