(define-module (systems alaok guix email)
  #:use-module (gnu services)
  #:use-module (gnu services mail)
  #:use-module (guix gexp)
  #:export (email-services))

(define opensmtpd-service
  (service opensmtpd-service-type
	   (opensmtpd-configuration
	    (config-file (plain-file "smtpd.conf" "
pki mail.alaok.org key \"/etc/letsencrypt/live/alaok.org/privkey.pem\"
pki mail.alaok.org certificate \"/etc/letsencrypt/live/alaok.org/cert.pem\"

table aliases file:/etc/aliases
table passwd file:/etc/mail/passwd

listen on eth0 port 25 hostname mail.alaok.org tls pki mail.alaok.org
listen on eth0 port 587 hostname mail.alaok.org tls-require pki mail.alaok.org auth <passwd> mask-source

accept from any for domain \"alaok.org\" alias <aliases> deliver to maildir \"~/mails\"
accept from local for any relay
")))))

(define dovecot-service
  (dovecot-service #:config
		   (opaque-dovecot-configuration
		    (string "
protocols = imap
ssl = required
ssl_key = </etc/letsencrypt/live/alaok.org/privkey.pem
ssl_cert = </etc/letsencrypt/live/alaok.org/fullchain.pem
ssl_dh = </etc/mail/dh.pem
mail_location = maildir:~/mails
listen = *

userdb {
  driver = passwd
  args = blocking=no
}

passdb {
  driver = pam
  args =
}
"))))

(define mail-aliases-service
  (service mail-aliases-service-type
	   '()))

(define smtp-passwd-file
  (extra-special-file "/etc/mail/passwd"
		      (plain-file "mail-passwd" "
peter $6$q66YTpqkE6JjQ6GG$6x3YgCbSZ.c9u6c7oitVM0OyRR5dmBkRmUyRlv0g7F7OJ5TO4S8hXEcsfs2OUCl1erzBkOWSTtrz2ywxR5xzx.
wendy $6$pOJ09TSnFCwJ7qOn$ZpZ2nYOjle.cJUiLuvKC3LvA36CQOjqHfNcQU.qMTYLno0wCfIkn2Ypb5mNrezff0wdnI2R3zD8SivtWhW5Dj/
")))

(define dh-parameters-file
  (extra-special-file "/etc/mail/dh.pem"
		     (local-file "../keys/dh.pem")))

(define email-services
  (list opensmtpd-service
	dovecot-service
	mail-aliases-service
	smtp-passwd-file
	dh-parameters-file))
