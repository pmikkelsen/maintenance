(define-module (systems alaok guix config)
  #:use-module (gnu)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu services networking)
  #:use-module (gnu services mail)
  #:use-module (gnu services ssh)
  #:use-module (gnu services web)
  #:use-module (gnu services base)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  
  #:use-module (keys)
  #:use-module (systems alaok guix cgit)
  #:use-module (systems alaok guix ssl-certs)
  #:use-module (systems alaok guix services cert)
  #:use-module (systems alaok guix email))

(define %authorized-guix-keys
  '(%laptop-guix-signing-key
    %hydra.gnu.org-guix-signing-key
    %berlin.gnu.org-guix-signing-key))

(define %substitute-urls
  (list "https://berlin.guixsd.org"
	"https://mirror.hydra.gnu.org"
	"http://localhost:8080" ; This requires an SSH tunnel from my laptop
				; running guix-publish
	))

(define domain-names
  '("alaok.org"
    "hvornaarsesviigen.alaok.org"))

(define droplet-networking-service
  (static-networking-service "eth0" "174.138.12.255"
			     #:netmask "255.255.240.0"
			     #:gateway "174.138.0.1"
			     #:name-servers '("8.8.8.8" "8.8.4.4")))

(define ssh-service
  (let ((ssh-keys `(("peter" ,%laptop-ssh-key)
		    ("root"  ,%laptop-ssh-key))))
    (service openssh-service-type
	     (openssh-configuration
	      (permit-root-login 'without-password)
	      (authorized-keys ssh-keys)))))

(define web-server-service
  (service nginx-service-type
	   (nginx-configuration
	    (server-blocks
	     (map (lambda (domain)
		    (nginx-server-configuration
		     (server-name (list domain))
		     (listen '("443 ssl"))
		     (root (string-append "/srv/" domain))
		     (ssl-certificate
		      (certificate-path %alaok.org-certificate))
		     (ssl-certificate-key
		      (certificate-key-path %alaok.org-certificate))
		     (raw-content '("error_page 404 /404.html;"))))
		  domain-names)))))

(define (flatten x)
  (cond ((null? x) '())
	((not (pair? x)) (list x))
	(else (append (flatten (car x))
		      (flatten (cdr x))))))

(operating-system
 (host-name "alaok.org")
 (timezone "Europe/Amsterdam")
 (locale "en_US.UTF-8")
 (bootloader (bootloader-configuration
	      (bootloader grub-bootloader)
	      (target "/dev/vda")))
 (file-systems (cons (file-system
		      (device "/dev/vda1")
		      (mount-point "/")
		      (type "ext4"))
		     %base-file-systems))
 (packages (cons* nss-certs
		  %base-packages))
 (users (cons* (user-account
		(name "peter")
		(group "users")
		(supplementary-groups '("wheel"))
		(home-directory "/home/peter"))
	       (user-account
		(name "wendy")
		(group "users")
		(home-directory "/home/wendy"))
	       %base-user-accounts))
 (services (flatten
	    (cons* droplet-networking-service
		   ssh-service
		   web-server-service
		   ssl-certificate-service
		   cgit-service
		   email-services
		   (modify-services
		    %base-services
		    (guix-service-type config =>
				       (guix-configuration
					(inherit config)
					(authorized-keys
					 %authorized-guix-keys)
					(substitute-urls
					 %substitute-urls))))))))
