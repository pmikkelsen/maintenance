(define-module (systems alaok guix ssl-certs)
  #:use-module (systems alaok guix services cert)
  #:export (%alaok.org-certificate
	    ssl-certificate-service))

(define %alaok.org-certificate
  (certificate
   (domain-name "alaok.org")
   (sub-domains '("git" "hvornaarsesviigen" "mail"))))

(define ssl-certificate-service
  (make-certificate-service (list %alaok.org-certificate)))
