(define-module (systems alaok guix cgit)
  #:use-module (systems alaok guix services git)
  #:use-module (systems alaok guix services cert)
  #:use-module (systems alaok guix ssl-certs)
  #:export (cgit-service))

(define %maintenance-repo
  (git-repo-config
   (path "maintenance.git")
   (name "System Maintenance")
   (url "maintenance")
   (description "Configuration files and keys for my systems")))

(define %website-repo
  (git-repo-config
   (path "website.git")
   (name "Alaok.org website")
   (url "website")
   (description "The source for the website at alaok.org")))

(define cgit-service
  (make-cgit-service
   (cgit-config
    (server-name "git.alaok.org")
    (ssl-cert %alaok.org-certificate)
    (repo-dir "/srv/git")
    (repo-list (list %maintenance-repo
                     %website-repo)))))


