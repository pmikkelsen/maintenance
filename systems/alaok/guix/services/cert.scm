(define-module (systems alaok guix services cert)
  #:use-module (gnu services)
  #:use-module (gnu services certbot)
  #:use-module (guix records)
  #:export (certificate

	    certificate-path
	    certificate-key-path
	    
	    make-certificate-service))

(define-record-type* <certificate> certificate
  make-certificate certificate?
  (domain-name certificate-domain-name (default ""))
  (sub-domains certificate-sub-domains (default '())))

(define (certificate->certbot-cert-config cert)
  (let* ((domain (certificate-domain-name cert))
	 (sub-domains (map (lambda (sub-part)
			     (string-append sub-part "." domain))
			   (certificate-sub-domains cert))))
    (certificate-configuration
     (name domain)
     (domains (cons domain sub-domains)))))

(define (certificate-path cert)
  (string-append "/etc/letsencrypt/live/" (certificate-domain-name cert)
		 "/fullchain.pem"))

(define (certificate-key-path cert)
  (string-append "/etc/letsencrypt/live/" (certificate-domain-name cert)
		 "/privkey.pem"))

(define (make-certificate-service cert-list)
  (service certbot-service-type
	   (certbot-configuration
	    (email "petermikkelsen10@gmail.com")
	    (webroot "/srv/certbot")
	    (certificates (map certificate->certbot-cert-config cert-list)))))
