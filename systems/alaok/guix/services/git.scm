(define-module (systems alaok guix services git)
  #:use-module (gnu services)
  #:use-module (gnu services cgit)
  #:use-module (gnu services web)
  #:use-module (guix records)
  #:use-module (systems alaok guix services cert)
  #:export (git-repo-config
	    cgit-config
	    make-cgit-service))

(define-record-type* <git-repo-config> git-repo-config make-git-repo-config
  git-repo-config?
  (path        git-repo-config-path        (default ""))
  (name        git-repo-config-name        (default ""))
  (url         git-repo-config-url         (default ""))
  (description git-repo-config-description (default "")))

(define-record-type* <cgit-config> cgit-config make-cgit-config
  cgit-config?
  (server-name  cgit-config-server-name  (default ""))
  (ssl-cert     cgit-config-ssl-cert     (default #f))
  (repo-list    cgit-config-repo-list    (default '()))
  (repo-dir     cgit-config-repo-dir     (default "/srv/git")))

(define (repo-config->cgit-repo-config cgit-config repo-config)
  (repository-cgit-configuration
   (path (string-append (cgit-config-repo-dir cgit-config) "/"
			(git-repo-config-path repo-config)))
   (name (git-repo-config-name        repo-config))
   (url  (git-repo-config-url         repo-config))
   (desc (git-repo-config-description repo-config))))

(define (make-cgit-service config)
  (service
   cgit-service-type
   (cgit-configuration
    (repositories (map (lambda (git-repo)
			 (repo-config->cgit-repo-config config git-repo))
		       (cgit-config-repo-list config)))
    (repository-directory "")
    (clone-prefix (let ((server-name (cgit-config-server-name config))
			(repo-dir    (cgit-config-repo-dir config)))
		    (list (string-append "ssh://" server-name
					 ":" repo-dir)
			  (string-append "https://" server-name))))
    (nginx (list (nginx-server-configuration
		  (inherit %cgit-configuration-nginx)
		  (listen '("443 ssl"))
		  (server-name (list (cgit-config-server-name config)))
		  (ssl-certificate
		   (certificate-path (cgit-config-ssl-cert config)))
		  (ssl-certificate-key
		   (certificate-key-path (cgit-config-ssl-cert config)))))))))
