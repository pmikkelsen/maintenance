(define-module (keys)
  #:use-module (guix gexp)
  #:export (%hydra.gnu.org-guix-signing-key
	    %berlin.guixsd.org-guix-signing-key
	    %laptop-guix-signing-key
	    %alaok-guix-signing-key

	    %laptop-ssh-key))

(define %hydra.gnu.org-guix-signing-key
  (local-file "misc/keys/guix/hydra.gnu.org.pub"))

(define %berlin.guixsd.org-guix-signing-key
  (local-file "misc/keys/guix/berlin.guixsd.org.pub"))

(define %laptop-guix-signing-key
  (local-file "systems/laptop/keys/guix/signing-key.pub"))

(define %alaok-guix-signing-key
  (local-file "systems/alaok/keys/guix/signing-key.pub"))

(define %laptop-ssh-key
  (local-file "systems/laptop/keys/ssh/id_rsa.pub"))
